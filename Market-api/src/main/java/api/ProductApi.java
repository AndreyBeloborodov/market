package api;

import dto.request.ProductRequest;
import dto.response.ProductResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;
import java.util.UUID;

@Api(tags = "Products | Продукты", value = "Продукт")
@RequestMapping("/products")
public interface ProductApi {

    @ApiOperation(value = "Добавление продукта", nickname = "product-create", response = Void.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Продукт добавлен", response = UUID.class),
            @ApiResponse(code = 400, message = "Ошибка валидации")})
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    void create(@RequestBody ProductRequest request);

    @PutMapping("/{productId}")
    @ResponseStatus(HttpStatus.OK)
    ProductResponse update(@PathVariable UUID productId, @RequestBody ProductRequest request);

    @DeleteMapping("/{productId}")
    @ResponseStatus(HttpStatus.OK)
    void deleteById(@PathVariable UUID productId);

    @GetMapping("/{productId}")
    @ResponseStatus(HttpStatus.OK)
    ProductResponse getById(@PathVariable UUID productId);

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    List<ProductResponse> getAll();
}
