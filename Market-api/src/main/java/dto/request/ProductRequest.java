package dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "реквест")
public class ProductRequest {

    @ApiModelProperty(value = "название")
    private String title;

    @ApiModelProperty(value = "цена")
    private Integer price;

    @ApiModelProperty(value = "колличество")
    private Integer count;
}
