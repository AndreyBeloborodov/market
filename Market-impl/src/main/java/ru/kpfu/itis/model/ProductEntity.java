package ru.kpfu.itis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Entity
@Table(name = "products")
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class ProductEntity extends AbstractEntity {

    @Column(nullable = false, unique = true)
    private String title;

    @Column(name = "price", nullable = false)
    private Integer price;

    @Column(name = "count")
    private Integer count;
}
