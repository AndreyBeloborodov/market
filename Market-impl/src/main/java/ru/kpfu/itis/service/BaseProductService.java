package ru.kpfu.itis.service;

import ru.kpfu.itis.config.property.DatabaseProperties;
import ru.kpfu.itis.exception.ProductNotFoundException;
import ru.kpfu.itis.mapper.ProductMapper;
import ru.kpfu.itis.model.ProductEntity;
import ru.kpfu.itis.repository.ProductRepository;
import ru.kpfu.itis.mapper.ProductMapper;
import dto.request.ProductRequest;
import dto.response.ProductResponse;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BaseProductService implements ProductService {

    private final ProductRepository repository;
    private final ProductMapper mapper;

    @Override
    public void create(ProductRequest request) {
        repository.save(mapper.toEntity(request));
    }

    @Override
    public ProductResponse update(UUID userId, ProductRequest request) {
        return null;
    }

    @Override
    public void deleteById(UUID userId) {
        repository.deleteById(userId);
    }

    @Override
    public ProductResponse getById(UUID userId) throws ProductNotFoundException {
        return repository.findById(userId)
                .map(mapper::toResponse)
                .orElseThrow(() -> new ProductNotFoundException(userId));
    }

    @Override
    public List<ProductResponse> getAll() {
        return repository.findAll().stream().map(mapper::toResponse).toList();
    }
}
