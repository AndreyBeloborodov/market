package ru.kpfu.itis.service;

import dto.request.ProductRequest;
import dto.response.ProductResponse;
import ru.kpfu.itis.exception.ProductNotFoundException;

import java.util.List;
import java.util.UUID;

public interface ProductService {
    void create(ProductRequest request);

    ProductResponse update(UUID userId, ProductRequest request);

    void deleteById(UUID userId);

    ProductResponse getById(UUID userId) throws ProductNotFoundException;

    List<ProductResponse> getAll();
}
