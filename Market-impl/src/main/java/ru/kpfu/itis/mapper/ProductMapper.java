package ru.kpfu.itis.mapper;

import ru.kpfu.itis.model.ProductEntity;
import dto.request.ProductRequest;
import dto.response.ProductResponse;
import liquibase.util.MD5Util;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.UUID;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    @Mapping(target = "id", ignore = true)
    ProductEntity toEntity(ProductRequest request);

    ProductResponse toResponse(ProductEntity entity);

    @Named("codePassword")
    default String codePassword(String password) {
        return MD5Util.computeMD5(password);
    }
}
