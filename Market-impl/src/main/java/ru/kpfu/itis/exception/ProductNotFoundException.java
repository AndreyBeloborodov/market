package ru.kpfu.itis.exception;

import javassist.NotFoundException;

import java.util.UUID;

public class ProductNotFoundException extends NotFoundException {
    public ProductNotFoundException(UUID userId) {
        super(String.format("User with this id = %s, not found", userId));
    }
}
