package ru.kpfu.itis.controllers;

import api.ProductApi;
import lombok.SneakyThrows;
import ru.kpfu.itis.service.ProductService;
import dto.request.ProductRequest;
import dto.response.ProductResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class ProductController implements ProductApi {

    private final ProductService service;

    @Override
    public void create(ProductRequest request) {
        service.create(request);
    }

    @Override
    public ProductResponse update(UUID productId, ProductRequest request) {
        return service.update(productId, request);
    }

    @Override
    public void deleteById(UUID productId) {
        service.deleteById(productId);
    }

    @SneakyThrows
    @Override
    public ProductResponse getById(UUID productId) {
        return service.getById(productId);
    }

    @Override
    public List<ProductResponse> getAll() {
        return service.getAll();
    }
}
